window.onload = function () {
    let result = {};
    let step = 0;
    function showQuestion(questionNumber) {
        document.querySelector('.question').innerHTML = quiz[step]['q'];
        let answer = '';
        for (let key in quiz[step]['a']) {
            answer += `<li data-v='${key}' class="answer-variant">${quiz[step]['a'][key]}</li>`;
        }
        document.querySelector('.answer').innerHTML = answer;

    }

    document.onclick = function (event) {
        event.stopPropagation();
        if (event.target.classList.contains('answer-variant') && step < quiz.length) {
            // event.target.data
            if (result[event.target.dataset.v] != undefined) {
                result[event.target.dataset.v]++;
            }
            else {
                result[event.target.dataset.v] = 0;
            }
            step++;
            if (step == quiz.length) {
                document.querySelector('.question').remove();
                document.querySelector('.answer').remove();
                document.querySelector('.talk').remove();
                document.querySelector('.screentonTest').remove();
                document.querySelector('.questionItems').remove();
                showResult();
            }
            else {
                showQuestion();
            }
        }
        if (event.target.classList.contains('reload-button')) {
            location.reload();
        }
    }

    function showResult() {
        let key = Object.keys(result).reduce(function (a, b) { return result[a] > result[b] ? a : b });

        // создаем общий див для верха финала
        let upFinal = document.createElement('div');
        upFinal.classList.add('upFinal');
        document.querySelector('main').appendChild(upFinal);

        //создали див YOU ARE
        let YouAre = document.createElement('div');
        YouAre.classList.add('YouAre');
        YouAre.innerHTML = answers[key]['YouAre'];
        document.querySelector('.upFinal').appendChild(YouAre);

        // создали див для каритнки и имени персонажа
        let personContent = document.createElement('div');
        personContent.classList.add('personContent');
        document.querySelector('.upFinal').appendChild(personContent);

        //закидываем картинки и имя персонажа в один див
        let window = document.createElement('img');
        window.src = 'images/' + answers[key]['window'];
        window.classList.add('windowImg');
        document.querySelector('.personContent').appendChild(window);

        let img2 = document.createElement('img');
        img2.src = 'images/' + answers[key]['image'];
        img2.classList.add('result-img');
        document.querySelector('.personContent').appendChild(img2);

        let person = document.createElement('div');
        person.classList.add('person');
        person.innerHTML = answers[key]['person'];
        document.querySelector('.personContent').appendChild(person);




        let downFinal = document.createElement('div');
        downFinal.classList.add('downFinal');
        document.querySelector('main').appendChild(downFinal);

        let div = document.createElement('div');
        div.classList.add('result');
        div.innerHTML = answers[key]['description'];
        document.querySelector('.downFinal').appendChild(div);

        let downTxt = document.createElement('div');
        downTxt.classList.add('downTxt');
        downTxt.innerHTML = answers[key]['downTxt'];
        document.querySelector('.downFinal').appendChild(downTxt);

        let invite = document.createElement('div');
        invite.classList.add('invite');
        invite.innerHTML = answers[key]['invite'];
        document.querySelector('.downFinal').appendChild(invite);

        document.querySelector('main').classList.add('FinalMain');

        // let reloadButton = document.createElement('button');
        // reloadButton.innerHTML = 'New quiz';
        // reloadButton.classList.add('reload-button');
        // document.querySelector('main').appendChild(reloadButton);
    }

    showQuestion();

}

